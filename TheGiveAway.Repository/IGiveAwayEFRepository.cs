﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGiveAway.Repository.Entities;

namespace TheGiveAway.Repository
{
    public interface IGiveAwayRepository
    {
        RepositoryActionResult<TheGiveAway.Repository.Entities.Item> DeleteItem(int id);
        RepositoryActionResult<TheGiveAway.Repository.Entities.ItemImage> DeleteItemImage(int id);
        RepositoryActionResult<TheGiveAway.Repository.Entities.DropOffPoint> DeleteDropOffPoint(int id);
        RepositoryActionResult<TheGiveAway.Repository.Entities.DropOffPointImage> DeleteDropOffPointImage(int id);
        RepositoryActionResult<TheGiveAway.Repository.Entities.ItemStatus> DeleteItemStatus(int id);

        TheGiveAway.Repository.Entities.Item GetItem(int id);
        TheGiveAway.Repository.Entities.Item GetItem(int id, string userId);
        System.Linq.IQueryable<TheGiveAway.Repository.Entities.Item> GetItems();
        System.Linq.IQueryable<TheGiveAway.Repository.Entities.Item> GetItemsWithImages();
        System.Linq.IQueryable<TheGiveAway.Repository.Entities.Item> GetItems(string userId);
        TheGiveAway.Repository.Entities.Item GetItemWithImages(int id);
        TheGiveAway.Repository.Entities.Item GetItemWithImages(int id, string userId);

        TheGiveAway.Repository.Entities.ItemStatus GetItemStatus(int id);
        System.Linq.IQueryable<TheGiveAway.Repository.Entities.ItemStatus> GetItemStatusses();
        TheGiveAway.Repository.Entities.DropOffPoint GetDropOffPoint(int id);
        System.Linq.IQueryable<TheGiveAway.Repository.Entities.DropOffPoint> GetDropOffPoints();
        TheGiveAway.Repository.Entities.DropOffPoint GetDropOffPointWithImages(int id);
        TheGiveAway.Repository.Entities.ItemImage GetItemImage(int id, int? itemId = null);
        TheGiveAway.Repository.Entities.DropOffPointImage GetDropOffPointImage(int id, int? dropOffPointId = null);

        RepositoryActionResult<TheGiveAway.Repository.Entities.Item> InsertItem(TheGiveAway.Repository.Entities.Item i);
        RepositoryActionResult<TheGiveAway.Repository.Entities.ItemImage> InsertItemImage(TheGiveAway.Repository.Entities.ItemImage im);
        RepositoryActionResult<TheGiveAway.Repository.Entities.DropOffPoint> InsertDropOffPoint(TheGiveAway.Repository.Entities.DropOffPoint d);
        RepositoryActionResult<TheGiveAway.Repository.Entities.DropOffPointImage> InsertDropOffPointImage(TheGiveAway.Repository.Entities.DropOffPointImage di);


        RepositoryActionResult<TheGiveAway.Repository.Entities.Item> UpdateItem(TheGiveAway.Repository.Entities.Item i);
        RepositoryActionResult<TheGiveAway.Repository.Entities.ItemImage> UpdateItemImage(TheGiveAway.Repository.Entities.ItemImage im);
        RepositoryActionResult<TheGiveAway.Repository.Entities.DropOffPoint> UpdateDropOffPoint(TheGiveAway.Repository.Entities.DropOffPoint d);
        RepositoryActionResult<TheGiveAway.Repository.Entities.DropOffPointImage> UpdateDropOffPointImage(TheGiveAway.Repository.Entities.DropOffPointImage di);

    }
}
