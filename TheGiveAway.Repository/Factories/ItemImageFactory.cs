﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGiveAway.Repository.Entities;

namespace TheGiveAway.Repository.Factories
{
    public class ItemImageFactory
    {
        public ItemImageFactory()
        {
        }

        public DTO.ItemImage CreateItemImage(ItemImage image)
        {
            return new DTO.ItemImage()
            {
                Id = image.Id, ImageName = image.ImageName, ItemId = image.ItemId
            };
        }

        public ItemImage CreateItemImage(DTO.ItemImage image)
        {
            return new ItemImage()
            {
                Id = image.Id,
                ImageName = image.ImageName,
                ItemId = image.ItemId
            };
        }

        public object CreateDataShapedObject(ItemImage image, List<string> listOfFields)
        {
            return CreateDataShapedObject(CreateItemImage(image), listOfFields);
        }

        public object CreateDataShapedObject(DTO.ItemImage itemImage, List<string> listOfFields)
        {
            if (!listOfFields.Any())
            {
                return itemImage;
            }

            ExpandoObject obj = new ExpandoObject();
            foreach (var field in listOfFields)
            {
                var fieldValue = itemImage.GetType()
                    .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                    .GetValue(itemImage, null);

                ((IDictionary<string, object>)obj).Add(field, fieldValue);
            }

            return obj;
        }
    }
}
