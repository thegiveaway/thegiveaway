﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGiveAway.Repository.Entities;
using TheGiveAway.Repository.Helpers;

namespace TheGiveAway.Repository.Factories
{
    public class ItemFactory
    {
        readonly ItemImageFactory _itemImageFactory = new ItemImageFactory();

        public ItemFactory()
        {

        }

        public Item CreateItem(DTO.Item item)
        {
            return new Item()
            {
                Id = item.Id,
                DateCreated = item.DateCreated == DateTime.MinValue ? DateTime.Now : item.DateCreated,
                DateGivenAway = item.DateGivenAway,
                Description = item.Description,
                ItemStatusId = item.ItemStatusId,
                UserId = item.UserId,
                Images = item.Images?.Select(x => _itemImageFactory.CreateItemImage(x)).ToList() ?? new List<ItemImage>()
            };
        }

        public DTO.Item CreateItem(Item item)
        {
            return new DTO.Item()
            {
                Id = item.Id,
                DateCreated = item.DateCreated,
                DateGivenAway = item.DateGivenAway,
                Description = item.Description,
                ItemStatusId = item.ItemStatusId,
                UserId = item.UserId,
                Images = item.Images.Select(x => _itemImageFactory.CreateItemImage(x)).ToList()
            };
        }

        public object CreateDataShapedObject(Item item, List<string> listOfFields)
        {
            return CreateDataShapedObject(CreateItem(item), listOfFields);
        }

        public object CreateDataShapedObject(DTO.Item item, List<string> listOfFields)
        {
            var listOfFieldsToWorkWith = new List<string>(listOfFields);

            if (!listOfFields.Any()) return item;
            
            var lstOfImageFields = listOfFieldsToWorkWith.Where(x => x.Contains("images")).ToList();

            var returnPartialItemImage = lstOfImageFields.Any() && !lstOfImageFields.Contains("images");

            if (returnPartialItemImage)
            {
                listOfFieldsToWorkWith.RemoveRange(lstOfImageFields);
                lstOfImageFields = lstOfImageFields.Select(x => x.Substring(x.IndexOf(".", StringComparison.Ordinal) + 1))
                    .ToList();
            }
            else
            {
                lstOfImageFields.Remove("images");
                listOfFieldsToWorkWith.RemoveRange(lstOfImageFields);
            }

            var objectToReturn = new ExpandoObject();
            foreach (var field in listOfFieldsToWorkWith)
            {
                var fieldValue = item.GetType()
                    .GetProperty(field,
                        BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public)
                    ?.GetValue(item, null);

                ((IDictionary<string, object>)objectToReturn).Add(field, fieldValue);
            }

            if (!returnPartialItemImage) return objectToReturn;

            var itemImages = item.Images
                .Select(itemImage => _itemImageFactory.CreateDataShapedObject(itemImage, lstOfImageFields))
                .ToList();

            ((IDictionary<string, object>)objectToReturn).Add("itemImages", itemImages);

            return objectToReturn;
        }
    }
}
