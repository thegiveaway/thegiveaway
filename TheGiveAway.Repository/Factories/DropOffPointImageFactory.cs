﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGiveAway.Repository.Entities;

namespace TheGiveAway.Repository.Factories
{
    public class DropOffPointImageFactory
    {
        public DropOffPointImageFactory()
        {
            
        }

        public DTO.DropOffPointImage CreateDropOffPointImage(DropOffPointImage dropOffPointImage)
        {
            return new DTO.DropOffPointImage()
            {
                DropOffPointId = dropOffPointImage.DropOffPointId,
                Id = dropOffPointImage.Id,
                Name = dropOffPointImage.Name
            };
        }

        public DropOffPointImage CreateDropOffPointImage(DTO.DropOffPointImage dropOffPointImage)
        {
            return new DropOffPointImage()
            {
                DropOffPointId = dropOffPointImage.DropOffPointId,
                Id = dropOffPointImage.Id,
                Name = dropOffPointImage.Name
            };
        }

        public object CreateDataShapedObject(DropOffPointImage dopi, List<string> listOfFields)
        {
            return CreateDataShapedObject(CreateDropOffPointImage(dopi), listOfFields);
        } 

        public object CreateDataShapedObject(DTO.DropOffPointImage dopi, List<string> listOfFields)
        {
            if (!listOfFields.Any()) return dopi;

            ExpandoObject objectToReturn = new ExpandoObject();

            foreach (var field in listOfFields)
            {
                var fieldValue = dopi.GetType()
                    .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public)?
                    .GetValue(dopi, null);

                ((IDictionary<string, object>)objectToReturn).Add(field, fieldValue);
            }

            return objectToReturn;
        }
    }
}
