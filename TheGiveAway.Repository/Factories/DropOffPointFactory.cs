﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using TheGiveAway.Repository.Entities;
using TheGiveAway.Repository.Helpers;

namespace TheGiveAway.Repository.Factories
{
    public class DropOffPointFactory
    {
        readonly DropOffPointImageFactory _dopiFactory = new DropOffPointImageFactory();

        public DropOffPointFactory()
        {
            
        }

        public DTO.DropOffPoint CreateDropOffPoint(DropOffPoint dropOffPoint)
        {
            return new DTO.DropOffPoint()
            {
                Id = dropOffPoint.Id,
                Location = dropOffPoint.Location,
                Name = dropOffPoint.Name,
                Images = dropOffPoint.Images
                .Select(x => _dopiFactory.CreateDropOffPointImage(x))
                .ToList()
            };
        }

        public DropOffPoint CreateDropOffPoint(DTO.DropOffPoint dropOffPoint)
        {
            return new DropOffPoint()
            {
                Id = dropOffPoint.Id,
                Location = dropOffPoint.Location,
                Name = dropOffPoint.Name,
                Images = dropOffPoint.Images
                    .Select(x => _dopiFactory.CreateDropOffPointImage(x))
                    .ToList()
            };
        }

        public object CreateDataShapedObject(DropOffPoint dropOffPoint, List<string> listOfFields)
        {
            return CreateDataShapedObject(CreateDropOffPoint(dropOffPoint), listOfFields);
        }


        public object CreateDataShapedObject(DTO.DropOffPoint dropOffPoint, List<string> listOfFields)
        {
            if (!listOfFields.Any()) return dropOffPoint;

            List<string> listOfFieldsToWorkWith = new List<string>(listOfFields);
            var listOfImageFIelds = listOfFieldsToWorkWith.Where(x => x.Contains("images"))
                .ToList();

            bool returnPartialImage = listOfImageFIelds.Any() && !listOfImageFIelds.Contains("images");

            if (returnPartialImage)
            {
                listOfFieldsToWorkWith.RemoveRange(listOfImageFIelds);
                listOfImageFIelds = listOfImageFIelds.Select(x => x.Substring(x.IndexOf('.') + 1))
                    .ToList();
            }
            else
            {
                listOfImageFIelds.Remove("images");
                listOfFieldsToWorkWith.RemoveRange(listOfImageFIelds);
            }

            ExpandoObject objectToReturn = new ExpandoObject();
            foreach (var field in listOfFieldsToWorkWith)
            {
                var fieldValue = dropOffPoint.GetType()
                    .GetProperty(field, BindingFlags.IgnoreCase | BindingFlags.Instance | BindingFlags.Public)
                    ?.GetValue(dropOffPoint, null);

                ((IDictionary<string, object>)objectToReturn).Add(field, fieldValue);
            }

            List<object> images = new List<object>();
            foreach (var image in dropOffPoint.Images)
            {
                images.Add(_dopiFactory.CreateDataShapedObject(image, listOfImageFIelds));
            }
            ((IDictionary<string, object>)objectToReturn).Add("images", images);

            return objectToReturn;
        }
    }
}
