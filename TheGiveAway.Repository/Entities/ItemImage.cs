﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TheGiveAway.Repository.Entities
{
    [Table("ItemImages")]
    public partial class ItemImage
    {
        public int Id { get; set; }

        public string ImageName { get; set; }

        public int ItemId { get; set; }

        public virtual Item Item { get; set; }
    }
}
