﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheGiveAway.Repository.Entities
{
    public partial class DropOffPoint
    {
        public DropOffPoint()
        {
            Images = new HashSet<DropOffPointImage>();
        }
        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        [Required, StringLength(250)]
        public string Location { get; set; }

        public virtual ICollection<DropOffPointImage> Images { get; set; }
    }
}
