﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGiveAway.Repository.Entities
{
    public partial class TheGiveAwayContext : DbContext
    {
        public TheGiveAwayContext()
            : base("name=TheGiveAway")
        {
        }

        public virtual DbSet<Item> Items { get; set; }
        public virtual DbSet<ItemImage> ItemImages { get; set; }
        public virtual  DbSet<DropOffPoint> DropOffPoints { get; set; }
        public virtual DbSet<DropOffPointImage> DropOffPointImages { get; set; }
        public virtual DbSet<ItemStatus> ItemStatuses { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
                .HasMany(x => x.Images)
                .WithRequired(x => x.Item)
                .WillCascadeOnDelete();

            modelBuilder.Entity<DropOffPoint>()
                .HasMany(x => x.Images)
                .WithRequired(x => x.DropOffPoint)
                .WillCascadeOnDelete();

            modelBuilder.Entity<ItemStatus>()
                .HasMany(x => x.Items)
                .WithRequired(x => x.ItemStatus)
                .HasForeignKey(x => x.ItemStatusId)
                .WillCascadeOnDelete(false);
        }
    }
}
