﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TheGiveAway.Repository.Entities
{
    public partial class ItemStatus
    {
        public ItemStatus()
        {
            Items = new HashSet<Item>();
        }

        public int Id { get; set; }

        [Required, StringLength(50)]
        public string Description { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
