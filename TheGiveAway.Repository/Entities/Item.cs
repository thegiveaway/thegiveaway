﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGiveAway.Repository.Entities
{
    [Table("Items")]
    public partial class Item
    {
        public Item()
        {
            Images = new HashSet<ItemImage>();

            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateGivenAway { get; set; }

        [Required, StringLength(100)]
        public string UserId { get; set; }

        public int ItemStatusId { get; set; }

        public virtual ICollection<ItemImage> Images { get; set; }
        public virtual ItemStatus ItemStatus { get; set; }
    }
}
