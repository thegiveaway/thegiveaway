﻿namespace TheGiveAway.Repository.Entities
{
    public partial class DropOffPointImage
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int DropOffPointId { get; set; }

        public virtual DropOffPoint DropOffPoint { get; set; }
    }
}
