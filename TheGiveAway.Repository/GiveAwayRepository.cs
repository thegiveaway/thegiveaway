﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TheGiveAway.Repository.Entities;

namespace TheGiveAway.Repository
{
    public class GiveAwayRepository: TheGiveAway.Repository.IGiveAwayRepository
    {
        private TheGiveAwayContext _ctx;

        public GiveAwayRepository(TheGiveAwayContext ctx)
        {
            _ctx = ctx;
            _ctx.Configuration.LazyLoadingEnabled = false;
        }

        public RepositoryActionResult<Item> DeleteItem(int id)
        {
            try
            {
                var item = _ctx.Items.Where(x => x.Id == id).FirstOrDefault();
                if (item == null)
                {
                    return new RepositoryActionResult<Item>(null, RepositoryActionStatus.NotFound);
                }
                return new RepositoryActionResult<Item>(null, RepositoryActionStatus.Deleted);
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<Item>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<ItemImage> DeleteItemImage(int id)
        {
            try
            {
                var ItemImage = _ctx.ItemImages.FirstOrDefault(x => x.Id == id);
                if (ItemImage == null)
                {
                    return new RepositoryActionResult<ItemImage>(null, RepositoryActionStatus.NotFound);
                }
                return new RepositoryActionResult<ItemImage>(null, RepositoryActionStatus.Deleted);
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<ItemImage>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<DropOffPoint> DeleteDropOffPoint(int id)
        {
            try
            {
                var DropOffPoint = _ctx.DropOffPoints.Where(x => x.Id == id).FirstOrDefault();
                if (DropOffPoint == null)
                {
                    return new RepositoryActionResult<DropOffPoint>(null, RepositoryActionStatus.NotFound);
                }
                return new RepositoryActionResult<DropOffPoint>(null, RepositoryActionStatus.Deleted);
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<DropOffPoint>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<DropOffPointImage> DeleteDropOffPointImage(int id)
        {
            try
            {
                var DropOffPointImage = _ctx.DropOffPointImages.FirstOrDefault(x => x.Id == id);
                if (DropOffPointImage == null)
                {
                    return new RepositoryActionResult<DropOffPointImage>(null, RepositoryActionStatus.NotFound);
                }
                return new RepositoryActionResult<DropOffPointImage>(null, RepositoryActionStatus.Deleted);
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<DropOffPointImage>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<ItemStatus> DeleteItemStatus(int id)
        {
            try
            {
                var ItemStatus = _ctx.ItemStatuses.FirstOrDefault(x => x.Id == id);
                if (ItemStatus == null)
                {
                    return new RepositoryActionResult<ItemStatus>(null, RepositoryActionStatus.NotFound);
                }
                return new RepositoryActionResult<ItemStatus>(null, RepositoryActionStatus.Deleted);
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<ItemStatus>(null, RepositoryActionStatus.Error, e);
            }
        }

        public Item GetItem(int id)
        {
            return _ctx.Items.FirstOrDefault(x => x.Id == id);
        }

        public Item GetItem(int id, string userId)
        {
            return _ctx.Items.FirstOrDefault(x => x.Id == id && x.UserId == userId);
        }

        public IQueryable<Item> GetItems()
        {
            return _ctx.Items;
        }

        public IQueryable<Item> GetItemsWithImages()
        {
            return _ctx.Items.Include("Images");  
        }

        public IQueryable<Item> GetItems(string userId)
        {
            return _ctx.Items.Where(x => x.UserId == userId);
        }

        public Item GetItemWithImages(int id)
        {
            return _ctx.Items.Include("Images").FirstOrDefault(x => x.Id == id);
        }

        public Item GetItemWithImages(int id, string userId)
        {
            return _ctx.Items.Include("Images").FirstOrDefault(x => x.Id == id && x.UserId == userId);
        }

        public ItemStatus GetItemStatus(int id)
        {
            return _ctx.ItemStatuses.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<ItemStatus> GetItemStatusses()
        {
            return _ctx.ItemStatuses;
        }

        public DropOffPoint GetDropOffPoint(int id)
        {
            return _ctx.DropOffPoints.FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<DropOffPoint> GetDropOffPoints()
        {
            return _ctx.DropOffPoints;
        }

        public IQueryable<DropOffPoint> GetDropOffPointsWithImages()
        {
            return _ctx.DropOffPoints.Include("Images");
        }

        public DropOffPoint GetDropOffPointWithImages(int id)
        {
            return _ctx.DropOffPoints.Include("Images").FirstOrDefault(x => x.Id == id);
        }

        public ItemImage GetItemImage(int id, int? itemId = null)
        {
            return _ctx.ItemImages.FirstOrDefault(x => x.Id == id && itemId == null || x.ItemId == itemId);
        }

        public DropOffPointImage GetDropOffPointImage(int id, int? dropOffPointId = null)
        {
            return _ctx.DropOffPointImages.FirstOrDefault(x => x.Id == id && dropOffPointId == null || x.DropOffPointId == dropOffPointId);
        }

        public RepositoryActionResult<Item> InsertItem(Item i)
        {
            try
            {
                _ctx.Items.Add(i);
                var result = _ctx.SaveChanges();
                if (result > 0)
                {
                    return new RepositoryActionResult<Item>(i, RepositoryActionStatus.Created);
                }
                else
                {
                    return new RepositoryActionResult<Item>(i, RepositoryActionStatus.NothingModified);
                }
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<Item>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<ItemImage> InsertItemImage(ItemImage im)
        {
            try
            {
                _ctx.ItemImages.Add(im);
                var result = _ctx.SaveChanges();
                if (result > 0)
                {
                    return new RepositoryActionResult<ItemImage>(im, RepositoryActionStatus.Created);
                }
                return new RepositoryActionResult<ItemImage>(im, RepositoryActionStatus.NothingModified);
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<ItemImage>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<DropOffPoint> InsertDropOffPoint(DropOffPoint d)
        {
            try
            {
                _ctx.DropOffPoints.Add(d);
                var result = _ctx.SaveChanges();
                if (result > 0) { return new RepositoryActionResult<DropOffPoint>(d, RepositoryActionStatus.Created);}
                else {  return  new RepositoryActionResult<DropOffPoint>(d, RepositoryActionStatus.NothingModified);}
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<DropOffPoint>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<DropOffPointImage> InsertDropOffPointImage(DropOffPointImage di)
        {
            try
            {
                _ctx.DropOffPointImages.Add(di);
                var result = _ctx.SaveChanges();
                if (result > 0) { return new RepositoryActionResult<DropOffPointImage>(di, RepositoryActionStatus.Created); }
                else { return new RepositoryActionResult<DropOffPointImage>(di, RepositoryActionStatus.NothingModified); }
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<DropOffPointImage>(null, RepositoryActionStatus.Error, e);
            }
        }


        public RepositoryActionResult<Item> UpdateItem(Item i)
        {
            try
            {
                var exisitngItem = _ctx.Items.FirstOrDefault(x => x.Id == i.Id);
                if (exisitngItem == null)
                {
                    return new RepositoryActionResult<Item>(i, RepositoryActionStatus.NotFound);
                }

                _ctx.Entry(i).State = EntityState.Detached;
                _ctx.Items.Attach(i);
                _ctx.Entry(i).State = EntityState.Modified;
                var result = _ctx.SaveChanges();

                if (result > 0)
                {
                    return new RepositoryActionResult<Item>(i, RepositoryActionStatus.Updated);
                }
                else {  return new RepositoryActionResult<Item>(i, RepositoryActionStatus.NothingModified, null);}
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<Item>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<ItemImage> UpdateItemImage(ItemImage im)
        {
            try
            {
                var exisitngItemImage = _ctx.ItemImages.FirstOrDefault(x => x.Id == im.Id);
                if (exisitngItemImage == null)
                {
                    return new RepositoryActionResult<ItemImage>(im, RepositoryActionStatus.NotFound);
                }

                _ctx.Entry(im).State = EntityState.Detached;
                _ctx.ItemImages.Attach(im);
                _ctx.Entry(im).State = EntityState.Modified;
                var result = _ctx.SaveChanges();

                if (result > 0)
                {
                    return new RepositoryActionResult<ItemImage>(im, RepositoryActionStatus.Updated);
                }
                else { return new RepositoryActionResult<ItemImage>(im, RepositoryActionStatus.NothingModified, null); }
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<ItemImage>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<DropOffPoint> UpdateDropOffPoint(DropOffPoint d)
        {
            try
            {
                var exisitngItemImage = _ctx.DropOffPoints.FirstOrDefault(x => x.Id == d.Id);
                if (exisitngItemImage == null)
                {
                    return new RepositoryActionResult<DropOffPoint>(d, RepositoryActionStatus.NotFound);
                }

                _ctx.Entry(d).State = EntityState.Detached;
                _ctx.DropOffPoints.Attach(d);
                _ctx.Entry(d).State = EntityState.Modified;
                var result = _ctx.SaveChanges();

                if (result > 0)
                {
                    return new RepositoryActionResult<DropOffPoint>(d, RepositoryActionStatus.Updated);
                }
                else { return new RepositoryActionResult<DropOffPoint>(d, RepositoryActionStatus.NothingModified, null); }
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<DropOffPoint>(null, RepositoryActionStatus.Error, e);
            }
        }

        public RepositoryActionResult<DropOffPointImage> UpdateDropOffPointImage(DropOffPointImage di)
        {
            try
            {
                var exisitngDropOffPointImage = _ctx.DropOffPointImages.FirstOrDefault(x => x.Id == di.Id);
                if (exisitngDropOffPointImage == null)
                {
                    return new RepositoryActionResult<DropOffPointImage>(di, RepositoryActionStatus.NotFound);
                }

                _ctx.Entry(di).State = EntityState.Detached;
                _ctx.DropOffPointImages.Attach(di);
                _ctx.Entry(di).State = EntityState.Modified;
                var result = _ctx.SaveChanges();

                if (result > 0)
                {
                    return new RepositoryActionResult<DropOffPointImage>(di, RepositoryActionStatus.Updated);
                }
                else { return new RepositoryActionResult<DropOffPointImage>(di, RepositoryActionStatus.NothingModified, null); }
            }
            catch (Exception e)
            {
                return new RepositoryActionResult<DropOffPointImage>(null, RepositoryActionStatus.Error, e);
            }
        }
    }
}
