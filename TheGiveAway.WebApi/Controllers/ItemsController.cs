﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Routing;
using TheGiveAway.Repository;
using TheGiveAway.Repository.Entities;
using TheGiveAway.Repository.Factories;
using TheGiveAway.WebApi.Helpers;

namespace TheGiveAway.WebApi.Controllers
{
    public class ItemsController : ApiController
    {
        private IGiveAwayRepository _repository;
        private ItemFactory _itemFactory = new ItemFactory();

        private const int maxPageSize = 10;

        public ItemsController()
        {
            _repository = new GiveAwayRepository(new TheGiveAwayContext());
        }

        public ItemsController(IGiveAwayRepository repository)
        {
            _repository = repository;
        }


        public IHttpActionResult Get(string fields = null, string sort = "id", string status = null,
            string userid = null, int page = 1, int pageSize = maxPageSize)
        {
            try
            {
                bool includeImages = false;
                List<string> lstOfFields = new List<string>();

                if (fields != null)
                {
                    lstOfFields = fields.ToLower().Split(',').ToList();
                    includeImages = lstOfFields.Any(x => x.Contains("images"));
                }

                int statusId = -1;
                if (status != null)
                {
                    switch (status.ToLower())
                    {
                        case "open":
                            statusId = 1;
                            break;
                        case "confirmed":
                            statusId = 2;
                            break;
                        case "processed":
                            statusId = 3;
                            break;
                        default: break;
                    }
                }

                IQueryable<Item> items = null;
                if (includeImages) {
                    items = _repository.GetItemsWithImages();
                }
                else {
                    items = _repository.GetItems();
                }

                items = items.ApplySort(sort)
                    .Where(x => (statusId == -1 || x.ItemStatusId == statusId))
                    .Where(x => (userid == null || x.UserId == userid));

                if (pageSize > maxPageSize)
                {
                    pageSize = maxPageSize;
                }

                var totalCount = items.Count();
                var totalPages = (int) Math.Ceiling((double) totalCount / pageSize);

                var urlhelper = new UrlHelper(Request);
                var prevLink = page > 1 ? urlhelper.Link("ItemsList", )

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


    }
}