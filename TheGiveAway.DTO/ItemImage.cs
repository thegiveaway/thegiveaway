﻿namespace TheGiveAway.DTO
{
    public class ItemImage
    {
        public int Id { get; set; }

        public string ImageName { get; set; }

        public int ItemId { get; set; }
    }
}
