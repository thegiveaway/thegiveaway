﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGiveAway.DTO
{
    public class DropOffPoint
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Location { get; set; }

        public ICollection<DropOffPointImage> Images { get; set; }
    }
}
