﻿using System.Collections.Generic;

namespace TheGiveAway.DTO
{
    public class ItemStatus
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
