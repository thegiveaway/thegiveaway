﻿namespace TheGiveAway.DTO
{
    public class DropOffPointImage
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int DropOffPointId { get; set; }
    }
}
