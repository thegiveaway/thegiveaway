﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheGiveAway.DTO
{
    public class Item
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime? DateGivenAway { get; set; }

        public string UserId { get; set; }

        public int ItemStatusId { get; set; }

        public ICollection<ItemImage> Images { get; set; }
    }
}
